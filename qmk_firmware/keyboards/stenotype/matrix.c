#include "quantum.h"
#include "process_steno.h"

void matrix_init_custom(void) {
    // Set I/O as pull-up inputs to read states
    setPinInputHigh(B1);  // #
    setPinInputHigh(B2);  // T
    setPinInputHigh(B3);  // P
    setPinInputHigh(B4);  // -G
    setPinInputHigh(B5);  // -S
    setPinInputHigh(B6);  // -Z
    setPinInputHigh(B7);  // H
    setPinInputHigh(D0);  // -L
    setPinInputHigh(D1);  // -P
    setPinInputHigh(D2);  // -F
    setPinInputHigh(D3);  // -T
    setPinInputHigh(D4);  // *
    setPinInputHigh(D5);  // -D
    setPinInputHigh(D6);  // -R
    setPinInputHigh(D7);  // -B
    setPinInputHigh(E2);  // R
    setPinInputHigh(E6);  // S
    setPinInputHigh(F0);  // K
    setPinInputHigh(F1);  // W
    setPinInputHigh(F4);  // A
    setPinInputHigh(F5);  // O
    setPinInputHigh(F6);  // E
    setPinInputHigh(F7);  // U

    steno_init();
    // setup steno mode (TX BOLT protocol)
    steno_set_mode(STENO_MODE_BOLT);
    matrix_init_quantum();
}

bool matrix_scan_custom(matrix_row_t current_matrix[]) {
    matrix_row_t next_matrix = readPin(B2) ? 0 : 1;
    next_matrix |= readPin(B3) ? 0 : 1 << 1;
    next_matrix |= readPin(B7) ? 0 : 1 << 2;
    next_matrix |= readPin(D2) ? 0 : 1 << 3;
    next_matrix |= readPin(D1) ? 0 : 1 << 4;
    next_matrix |= readPin(D0) ? 0 : 1 << 5;
    next_matrix |= readPin(D3) ? 0 : 1 << 6;
    next_matrix |= readPin(D5) ? 0 : 1 << 7;
    bool matrix_has_changed = current_matrix[0] != next_matrix;
    current_matrix[0] = next_matrix;
    next_matrix = readPin(F0) ? 0 : 1;
    next_matrix |= readPin(F1) ? 0 : 1 << 1;
    next_matrix |= readPin(E2) ? 0 : 1 << 2;
    next_matrix |= readPin(D6) ? 0 : 1 << 3;
    next_matrix |= readPin(D7) ? 0 : 1 << 4;
    next_matrix |= readPin(B4) ? 0 : 1 << 5;
    next_matrix |= readPin(B5) ? 0 : 1 << 6;
    next_matrix |= readPin(B6) ? 0 : 1 << 7;
    matrix_has_changed = matrix_has_changed || current_matrix[1] != next_matrix;
    current_matrix[1] = next_matrix;
    next_matrix = readPin(E6) ? 0 : 1;
    next_matrix |= readPin(F4) ? 0 : 1 << 1;
    next_matrix |= readPin(F5) ? 0 : 1 << 2;
    next_matrix |= readPin(F6) ? 0 : 1 << 3;
    next_matrix |= readPin(F7) ? 0 : 1 << 4;
    next_matrix |= readPin(B1) ? 0 : 1 << 5;
    next_matrix |= readPin(D4) ? 0 : 1 << 6;
    matrix_has_changed = matrix_has_changed || current_matrix[2] != next_matrix;
    current_matrix[2] = next_matrix;

    matrix_scan_quantum();

    return matrix_has_changed;
}
