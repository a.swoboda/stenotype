#include "../../stenotype.h"
#include "keymap_steno.h"
#include "../../config.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	LAYOUT(
		STN_TL, STN_PL, STN_HL, STN_FR, STN_PR, STN_LR, STN_TR, STN_DR, STN_KL, STN_WL, STN_RL, STN_RR, STN_BR, STN_GR, STN_SR, STN_ZR, STN_SL, STN_A, STN_O, STN_E, STN_U, STN_NUM, STN_STR
	)
};
