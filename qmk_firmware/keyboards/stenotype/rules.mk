MCU = atmega32u4

# Bootloader selection
BOOTLOADER = atmel-dfu

# Enable Steno. Disable mouse reports to make sure there are enough USB endpoints available
STENO_ENABLE = yes
MOUSEKEY_ENABLE = no

# Need a custom matrix, since all switches are wired directly
CUSTOM_MATRIX = lite

SRC += matrix.c

