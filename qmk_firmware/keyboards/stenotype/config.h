#ifndef CONFIG_H
#define CONFIG_H

#include "config_common.h"

/* USB Device descriptor parameter */
#define VENDOR_ID       0xFEED
#define PRODUCT_ID      0x6060
#define DEVICE_VER      0x0001
#define MANUFACTURER    aswoboda
#define PRODUCT         stenotype
#define DESCRIPTION     stenotype

/* key matrix size */
#define MATRIX_ROWS 3
#define MATRIX_COLS 8

/* Set 0 if debouncing isn't needed */
#define DEBOUNCING_DELAY 5

#define USB_POLLING_INTERVAL_MS 10

#endif
