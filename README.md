# Open Hardware Stenotype

This open hardware stenotype machine was designed to be a minimalist starting point in the world of stenotyping, e.g., with [Plover](https://www.openstenoproject.org/plover/) of the [Open Steno Project](https://www.openstenoproject.org/).  
It is designed to be cheap to print (2-layer PCB with pretty standard tolerances) and relatively easy to assemble (minimum number of parts, and easy enough to hand solder).

## Getting Started
In the future, I shall provide more details on all steps in the wiki.
1. Print the PCB
2. Get the parts
3. Assemble (solder) parts
4. Build the firmware (with [QMK](https://qmk.fm/); I will provide a download in the future)
5. Flash the firmware

## Layout
![](/stenotype-layout.png "Layout of the stenotype machine")

This is the standard layout for a stenotype machine, except for a twist: Instead of having a "#"-row on top, I have replaced the top left key (usually an extra "S") with it.
Imho, this significantly simplifies and slims down the design, and the left little finger is not that busy anyways.

## Future Plans
* STM32F0x2 as a replacement for the ATMEGA32u4. Simply because it needs less components. Alas, the global chip shortage...
* Improve reset. Currently, you need to press 'R' and short H1 and H2 if you want to flash new firmware
* A 3d printable case (FDM)
* Through-hole footprints for the switches. Compatibility with ALPS switches
* (QWERTY mode. Nice gimmick, but pretty useless once you know fingerspelling)

